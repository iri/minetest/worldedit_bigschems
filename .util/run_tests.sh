#!/usr/bin/env bash

set -Eeuo pipefail
trap cleanup SIGINT SIGTERM ERR EXIT

script_dir=$(cd "$(dirname "${BASH_SOURCE[0]}")" &>/dev/null && pwd -P)

usage() {
  cat <<EOF
Usage: $(basename "${BASH_SOURCE[0]}") [-h] [-D] [-f/--failfast] [-k/--keep-server] [-d/--debug] 
                          [-p/--port port_server] [--dependencies mod_dependencies]
                          [-c/--clients nb_of_client] [-i/--client-image client_docker_image]
                          [--docker-cmd docker_command]


Start tests.

Available options:

-h, --help          Print this help and exit
--docker-cmd        The container engine (docker or podman)
--podman            Use podman as container engine
--docker            Use docker as container engine
-D, --show-debug    Display debug.txt content on exit
-f, --failfast      Failfast tests
-k, --keep-server   Keep the server running
-p, --port          Server port
-c, --clients       Number of client, default to \$CLIENT_NB
--dependencies      Mod dependencies
-i, --client-image  Client docker image (default to registry.apps.education.fr/iri/minetest/docker_test_harness/client:latest)
-d, --debug         Debug the script
EOF
  exit
}

cleanup() {
  trap - SIGINT SIGTERM ERR EXIT
  if [ -f "$PWD/${docker_compose_file:-}" ] && ! [ -z "$docker_cmd" ]; then
    eval $docker_cmd compose -f "$PWD/${docker_compose_file}" down || true
  fi
  if [ -f "$PWD/${docker_compose_file:-}" ]; then
    \rm  "$PWD/${docker_compose_file}"
  fi
}

setup_colors() {
  if [[ -t 2 ]] && [[ -z "${NO_COLOR-}" ]] && [[ "${TERM-}" != "dumb" ]]; then
    NOFORMAT='\033[0m' RED='\033[0;31m' GREEN='\033[0;32m' ORANGE='\033[0;33m' BLUE='\033[0;34m' PURPLE='\033[0;35m' CYAN='\033[0;36m' YELLOW='\033[1;33m'
  else
    NOFORMAT='' RED='' GREEN='' ORANGE='' BLUE='' PURPLE='' CYAN='' YELLOW=''
  fi
}

msg() {
  echo >&2 -e "${1-}"
}

die() {
  local msg=$1
  local code=${2-1} # default exit status 1
  msg "$msg"
  exit "$code"
}

exists_in_list() {
  LIST=$1
  DELIMITER=$2
  VALUE=$3
  LIST_WHITESPACES=$(echo $LIST | tr "$DELIMITER" " ")
  for x in $LIST_WHITESPACES; do
    if [ "$x" = "$VALUE" ]; then
      return 0
    fi
  done
  return 1
}

isuint() { [[ $1 == +([0-9]) ]] ;}

docker_cmd=
docker_build_cmd="build"
client_image=
client_nb=${CLIENT_NB:-1}
failfast="false"
stop_server="true"
server_port="30000"
show_debug="false"
run_tests="all"
dependencies="${DEPENDENCIES:-}"
docker_compose_file=""
debug=false

parse_params() {
  # init switch flags
  args=$(getopt -a -o Dfhkdc:p:t:i: --long show-debug,failfast,keep-server,help,podman,docker,debug,clients:,client-image:,port:,docker-cmd:,tests:,dependencies: -- "$@")
  if [[ $? -gt 0 ]]; then
    usage
  fi

  eval set -- ${args}
  while [[ $# -gt 0 ]]; do
    case $1 in
    -f|--failfast)
      failfast="true"
      shift
      ;;
    -k|--keep-server)
      stop_server="false"
      shift
      ;;
    -h|--help)
      usage
      shift
      ;;
    --docker)
      docker_cmd="docker"
      shift
      ;;
    --podman)
      docker_cmd="podman"
     shift
      ;;
    -D|--show-debug)
      show_debug="true"
      shift
      ;;
    -d|--debug)
      debug=true
      shift
      ;;
    --docker-cmd)
      docker_cmd=$2
      shift 2
      ;;
    -p|--port)
      server_port=$2
      shift 2
      ;;
    -t|--tests)
      run_tests=$2
      shift 2
      ;;
    -i|--client-image)
      client_image=$2
      shift 2
      ;;
    -c|--clients)
      client_nb=$2
      shift 2
      ;;
    --dependencies)
      dependencies=$2
      shift 2
      ;;
    # -- means the end of the arguments; drop this, and break out of the while loop
    --)
      shift
      break
      ;;
    *)
      echo >&2 Unsupported option: $1
      usage
      ;;
    esac
  done

  shift $((OPTIND - 1))

  # check required params and arguments
  if [ "$debug" = "true" ]; then set -x; fi
  if [[ "$docker_cmd" == "docker" ]]; then docker_build_cmd="buildx build"; fi 
  if ! exists_in_list "docker podman", " ", "$docker_cmd"; then die "docker command must be in docker or podman"; fi
  if ! exists_in_list "all simple players", " ", "$run_tests"; then die "run tests (-t) must be in (all,simple,players)"; fi
  if ! isuint $client_nb; then die "Number of clients must be an integer"; fi

  return 0
}

parse_params "$@"
setup_colors

# script logic here

[ -f mod.conf ] || [ -f modpack.conf ] || {
  echo "Must be run in mod root folder." >&2
  exit 1
}

if [ "$docker_cmd" == "docker" ]; then
  command -v docker >/dev/null || {
    echo "Docker is not installed." >&2
    exit 1
  }
elif [ "$docker_cmd" == "podman" ]; then
  command -v podman >/dev/null || {
    echo "Podman is not installed." >&2
    exit 1
  }
fi


echo "STOP SERVER : $stop_server - SERVER PORT : ${server_port:-30000} - SHOW DEBUG : $show_debug"
port="--publish=\"${server_port:-30000}:30000/udp\" "
echo "PORT : $port"

vol=(
  -v "$PWD":/usr/local/share/minetest/games/minetest/mods/test_harness
)
[ -d minetest_game ] && vol+=(
  -v "$PWD/minetest_game":/var/lib/minetest/.minetest/games/minetest_game
)
export SHOW_DEBUG=${show_debug:-false}
export RUN_TESTS=${run_tests}
export STOP_SERVER="${stop_server:-true}"
export FAILFAST="${failfast:-false}"
export DEPENDENCIES="${dependencies}"
export CLIENT_IMAGE="${client_image:-registry.apps.education.fr/iri/minetest/docker_test_harness/client:latest}"
if ! $docker_cmd manifest inspect $CLIENT_IMAGE > /dev/null 2>&1 ; then
  die "CLIENT IMAGE $CLIENT_IMAGE does not exists"
fi

export CURRENT_MOD=""
export ADDITIONAL_MODS=""
export ADDITIONAL_FILES=""
if [ -f "$PWD/.mod_env.json" ]; then
  DEPENDENCIES="$(cat "$PWD/.mod_env.json" | jq -cr ".dependencies//\"\"")"
  CURRENT_MOD="$(cat "$PWD/.mod_env.json" | jq -cr ".current_mod//\"\"")"
  CLIENT_NB="$(cat "$PWD/.mod_env.json" | jq -cr ".client_nb//$client_nb")"
  ADDITIONAL_MODS="$(cat "$PWD/.mod_env.json" | jq -cr ".additional_mods//\"\"")"
  ADDITIONAL_FILES="$(cat "$PWD/.mod_env.json" | jq -cr ".additional_files//[]")"
fi

docker_compose_file=`mktemp --suffix=.yaml docker-composeXXXXXX --tmpdir=.util`

cat "${script_dir}/docker-compose.yaml" > "$PWD/$docker_compose_file"
for (( clienti=1; clienti<=$CLIENT_NB; clienti++ )); do
  cat <<EOF >> "$PWD/$docker_compose_file"
  client${clienti}:
    image: \${CLIENT_IMAGE}
    restart: on-failure:10
    depends_on:
      - "server"
    environment:
      SERVER: server
      PORT: 30000
      PLAYERNAME: player${clienti}
      RANDOM_INPUT: 0
      PASSWORD: test
EOF

done

if [ "$debug" = "true" ]; then
  echo -e "$GREEN------------------ $docker_compose_file ----------------------------------$NOFORMAT"
  cat "$PWD/$docker_compose_file"
  echo -e "$GREEN--------------------------------------------------------------------------$NOFORMAT"
fi

eval $docker_cmd compose -f "$PWD/${docker_compose_file}" down
eval $docker_cmd compose -f "$PWD/${docker_compose_file}" pull
eval $docker_cmd compose -f "$PWD/${docker_compose_file}" build --pull
eval $docker_cmd compose -f "$PWD/${docker_compose_file}" up --force-recreate --exit-code-from server --abort-on-container-exit
exit_code=$?

exit $exit_code