local MAX_FOLDER_NAME_LOOP = 10000
local STATE_KEY = "WORLDEDIT_BIGSCHEMS_STATE"
local storage = minetest.get_mod_storage()

local undo = {}

-- init load previous state
local previous_state = storage:get_string(STATE_KEY)

if previous_state ~= nil and previous_state ~= "" then
	undo.state = minetest.deserialize(previous_state)
else
	undo.state = {
		history = {},
		transactions = {}
	}
end

minetest.mkdir(worldedit_bigschems.history_path)
minetest.mkdir(worldedit_bigschems.transactions_path)

local function saveState()
	storage:set_string(STATE_KEY, minetest.serialize(undo.state))
end

local buildFolderName = function()
	local i = 0
	local folder_name = nil
	while i < MAX_FOLDER_NAME_LOOP and folder_name == nil do
		i = i + 1
		folder_name = worldedit_bigschems.random_string(20)
		local fcontentt = worldedit_bigschems.exists(worldedit_bigschems.transactions_path .. "/" .. folder_name)
		local fcontenth = worldedit_bigschems.exists(worldedit_bigschems.history_path .. "/" .. folder_name)
		if (fcontentt or fcontenth) then
			folder_name = nil
		end
	end
	return folder_name
end

function undo.clearTransactions()
	undo.state.transactions = {}
	os.remove(worldedit_bigschems.transactions_path)
	minetest.mkdir(worldedit_bigschems.transactions_path)
end

Transaction = {
	command = nil,
	folder_name = nil,
	data = nil,
}

-- start
function Transaction:new(o)
	o = o or {}
	if not o.folder_name then
		local folder_name = buildFolderName()
		if folder_name == nil then
			return nil
		end
		o.folder_name = folder_name
	end
	if not o.data then
		o.data = {}
	end
	if not o.command then
		return nil
	end
	setmetatable(o, self)
	self.__index = self
	table.insert(undo.state.transactions, o)
	return o
end

local function minAttr(o1, o2)
	if o2 == nil or next(o2) == nil then
		return o1
	end
	if o1 == nil or next(o1) == nil then
		return o2
	end
	local res = {}
	for k, v1 in pairs(o1) do
		local v2 = o2[k]
		if v1 == nil then
			res[k] = v2
		elseif v2 == nil then
			res[k] = v1
		else
			res[k] = math.min(v1, v2)
		end
	end
	return res
end

local function maxAttr(o1, o2)
	if o2 == nil or next(o2) == nil then
		return o1
	end
	if o1 == nil or next(o1) == nil then
		return o2
	end
	local res = {}
	for k, v1 in pairs(o1) do
		local v2 = o2[k]
		if v1 == nil then
			res[k] = v2
		elseif v2 == nil then
			res[k] = v1
		else
			res[k] = math.max(v1, v2)
		end
	end
	return res
end

local function minReductor(out, dataItem)
	return minAttr(out, minAttr(dataItem.pos_min, dataItem.pos_max))
end

local function maxReductor(out, dataItem)
	return maxAttr(out, maxAttr(dataItem.pos_min, dataItem.pos_max))
end


function Transaction:asTable()
	local res = {
		command = self.command,
		folder_name = self.folder_name,
		data = {}
	}
	for _, d in ipairs(self.data) do
		table.insert(res.data, d)
	end
	local length_region = { x = 0, y = 0, z = 0, is_bigmtschm = true }
	if next(res.data) ~= nil then
		local pos_min = worldedit_bigschems.reduce(res.data, minReductor,
			{ x = math.maxinteger, y = math.maxinteger, z = math.maxinteger })
		local pos_max = worldedit_bigschems.reduce(res.data, maxReductor,
			{ x = math.mininteger, y = math.mininteger, z = math.mininteger })
		length_region = { x = pos_max.x - pos_min.x, y = pos_max.y - pos_min.y, z = pos_max.z - pos_min.z, is_bigmtschm = true }
	end
	table.insert(res.data, 1, length_region)
	return res
end

function Transaction:getPath()
	return worldedit_bigschems.transactions_path .. "/" .. self.folder_name
end

local function ensurePath(self)
	local path = self:getPath()
	if not worldedit_bigschems.exists(path) then
		minetest.mkdir(path)
	end
	return path
end


-- TODO: order p1 p2 ???
function Transaction:checkpoint(pos1, pos2)
	local fname = #self.data + 1 .. ".mts"
	minetest.create_schematic(pos1, pos2, nil, ensurePath(self) .. "/" .. fname)
	table.insert(self.data, { pos_min = pos1, pos_max = pos2, name = fname })
	saveState()
end

function Transaction:findIndex()
	local self_index = nil
	for i, v in ipairs(undo.state.transactions) do
		if v.folder_name == self.folder_name then
			self_index = i
			break
		end
	end
	return self_index
end

function Transaction:commit()
	local history_path = worldedit_bigschems.history_path .. "/" .. self.folder_name
	if worldedit_bigschems.exists(history_path) then
		return false
	end
	local rename_res = os.rename(ensurePath(self), history_path)
	if not rename_res then
		return false
	end
	local hist_item = self:asTable()
	worldedit_bigschems.write_file(history_path .. "/data", hist_item.data)
	table.insert(undo.state.history, 1, hist_item)
	if #undo.state.history > worldedit_bigschems.history_max_undo then
		local last_hist = table.remove(undo.state.history, #undo.state.history)
		for _, d in ipairs(last_hist.data) do
			os.remove(history_path .. "/" .. last_hist.folder_name .. "/" .. d.name)
		end
		os.remove(history_path .. "/" .. last_hist.folder_name .. "/data")
		os.remove(history_path .. "/" .. last_hist.folder_name)
	end
	local self_index = self:findIndex()
	if self_index ~= nil then
		table.remove(undo.state.transactions, self_index)
	end
	saveState()
end

function Transaction:rollback()
	for i = #self.data, 1, -1 do
		local d_i = self.data[i]
		minetest.place_schematic(d_i.pos_min, ensurePath(self) .. "/" .. d_i.name)
	end
	-- delete transaction
	for _, d in ipairs(self.data) do
		os.remove(self:getPath() .. "/" .. d.name)
	end
	os.remove(ensurePath(self))
	local self_index = self:findIndex()
	if self_index ~= nil then
		table.remove(undo.state.transactions, self_index)
	end
	saveState()
end

undo.Transaction = Transaction
function undo.getTransaction(cmd)
	return Transaction:new { command = cmd }
end

-- t is a table
function undo.findTransaction(t)
	local self_index = nil
	for i, v in ipairs(undo.state.transactions) do
		if v.folder_name == t.folder_name then
			self_index = i
			break
		end
	end
	return self_index
end

function undo.removeTransaction(i)
	return table.remove(undo.state.transactions, i)
end

return undo
