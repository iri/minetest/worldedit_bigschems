local S = minetest.get_translator("worldedit_bigschems_undo")

worldedit_bigschems.modpath.worldedit_bigschems_undo = minetest.get_modpath("worldedit_bigschems_undo")

worldedit_bigschems.history_path = minetest.get_worldpath() .. "/schems/history"
worldedit_bigschems.transactions_path = minetest.get_worldpath() .. "/schems/transactions"
local mu = minetest.settings:get("worldedit_bigschems_max_undo")
worldedit_bigschems.history_max_undo = mu and tonumber(mu) or 10

local transactions_path = worldedit_bigschems.transactions_path

local undo = dofile(worldedit_bigschems.modpath.worldedit_bigschems_undo .. "/undo.lua")
worldedit_bigschems.undo = undo

local transactions_on_start = minetest.settings:get("worldedit_bigschems_transactions_on_start")
if transactions_on_start == nil then
	transactions_on_start = 'rollback'
end

if not string.find("|keep|delete|rollback|", transactions_on_start) then
	minetest.log('error', "the value of worldedit_bigschems_transactions_on_start ('"..transactions_on_start.."') is not in keep,delete,rollback. Set it to the default value 'rollback'.")
	transactions_on_start = 'rollback'
end
--TODO check path exists
minetest.mkdir(transactions_path)

if transactions_on_start == 'delete' then
	undo.clearTransactions()
elseif transactions_on_start == 'rollback' then
	local transactionList = {}
	for _,t in ipairs(undo.state.transactions) do
		table.insert(transactionList, t)
	end
	for i=#transactionList,1,-1 do
		local t = transactionList[i]
		local transIndex = undo.findTransaction(t)
		if transIndex ~= nil then
			undo.removeTransaction(transIndex)
			local trans = undo.Transaction.new(t)
			trans:rollback()
		end
	end
end

dofile(worldedit_bigschems.modpath.worldedit_bigschems_undo .. "/commands.lua")

minetest.register_chatcommand("/worldundo", {
	params = "<id>",
	description = S("Undo a world placement"),
	privs = { worldedit = true },
	func = worldedit_bigschems.worldundo
})

minetest.register_chatcommand("/worldjournal", {
	params = "",
	description = S("List logs that can be undone"),
	privs = { worldedit = true },
	func = worldedit_bigschems.worldjournal
})

if minetest.get_modpath("test_harness") and minetest.settings:get_bool("test_harness_run_tests", false) then
	dofile(worldedit_bigschems.modpath.worldedit_bigschems_undo .. "/test/init.lua")
end
