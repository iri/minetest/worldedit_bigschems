local S = minetest.get_translator("worldedit_bigschems_undo")

-- TODO: This does not remove the history item. Is it normal ?
function worldedit_bigschems.worldundo(name, param)
	local history = nil

	if not param then
		return false, S("Error with parameter")
	end


	local undo_index = tonumber(param)
	if not undo_index then
		return false, S("Error with parameter")
	end
	if undo_index <= #worldedit_bigschems.undo.state.history then
		history = worldedit_bigschems.undo.state.history[undo_index]
	else
		return false, S("Error with parameter")
	end
	minetest.chat_send_player(name, S("Begin to cancel"))
	local path = minetest.get_worldpath() .. "/schems/history/" .. history.folder_name .. "/"
	for i = #history.data, 2, -1 do
		local d = history.data[i]
		if not minetest.place_schematic(d.pos_min, path .. d.name) then
			return false, S("The undo failed")
		end
	end
	if history.area and minetest.get_modpath("areas") then
		areas:remove(history.area, true)
		areas:save()
	end
	return true, S("The undo succeed")
end

function worldedit_bigschems.worldjournal(name)
	local list = S("Here are the commands that you can undo (with their id)") .. "\n"
	for i, v in ipairs(worldedit_bigschems.undo.state.history) do
		list = list .. " [" .. i .. "] " .. v.command .. " ;\n"
	end
	return true, list
end
