local PLAYER_NAME = "Player"
local vec = vector.new

worldedit_bigschems.register_test("Test undo Commands")

worldedit_bigschems.register_test("test worldedit_bigschems.worldjournal", function()
    worldedit_bigschems.undo.state.history = {}

    for i = 1, worldedit_bigschems.history_max_undo do
        local t = worldedit_bigschems.undo.getTransaction("//DO_THIS_" .. i)
        t:commit()
    end

    local res,res_list = worldedit_bigschems.worldjournal(PLAYER_NAME)

    assert(res and res_list ~= nil)

    for i = 1, worldedit_bigschems.history_max_undo do
        local entry = "%[" .. worldedit_bigschems.history_max_undo + 1 - i .. "%] //DO_THIS_" .. i .. " ;"
        assert(string.find(res_list, entry))
    end
end)

worldedit_bigschems.register_test("test worldedit_bigschems.worldundo", function()
    worldedit_bigschems.undo.state.history = { {
        data = { { x = 56, y = 56, z = 56, is_bigmtschm = true }, { pos_min = { x = 0, y = 0, z = 0 }, pos_max = { x = 56, y = 56, z = 56 }, name = "1.mts" } },
        command = "//CMD",
        folder_name = "CMD"
    } }

    local test_mts_path = minetest.get_modpath("worldedit_bigschems_undo") .. "/test/history/1.mts"
    minetest.mkdir(worldedit_bigschems.history_path .. "/CMD")
    local source_file = assert(io.open(test_mts_path, 'rb'))
    local dest_file = assert(io.open(worldedit_bigschems.history_path .. "/CMD/1.mts", "wb"))
    dest_file:write(source_file:read("*all"))
    dest_file:close()
    source_file:close()

    local pattern = { testnode1, testnode3 }

    -- check that the area is empty
    check.not_filled(vec(0, 0, 0), vec(56, 56, 56), pattern)
    check.filled(vec(0, 0, 0), vec(56, 56, 56), "air")


    local res_cmd, msg = worldedit_bigschems.worldundo(PLAYER_NAME, "1")

    -- check that the area is filled with pattern
    assert(res_cmd == true)
    assert(msg ~= nil)
    check.pattern(vec(0, 0, 0), vec(56, 56, 56), pattern)

    -- empty area
    place_pattern(vec(0, 0, 0), vec(56, 56, 56), { "air" })

end)

worldedit_bigschems.register_test("test worldedit_bigschems.worldundo no arg", function()
    worldedit_bigschems.undo.state.history = { {
        data = { { x = 56, y = 56, z = 56, is_bigmtschm = true }, { pos_min = { x = 0, y = 0, z = 0 }, pos_max = { x = 56, y = 56, z = 56 }, name = "1.mts" } },
        command = "//CMD",
        folder_name = "CMD"
    } }

    local test_mts_path = minetest.get_modpath("worldedit_bigschems_undo") .. "/test/history/1.mts"
    minetest.mkdir(worldedit_bigschems.history_path .. "/CMD")
    local source_file = assert(io.open(test_mts_path, 'rb'))
    local dest_file = assert(io.open(worldedit_bigschems.history_path .. "/CMD/1.mts", "wb"))
    dest_file:write(source_file:read("*all"))
    dest_file:close()
    source_file:close()

    local pattern = { testnode1, testnode3 }

    -- check that the area is empty
    check.not_filled(vec(0, 0, 0), vec(56, 56, 56), pattern)
    check.filled(vec(0, 0, 0), vec(56, 56, 56), "air")


    local res_cmd, _ = worldedit_bigschems.worldundo(PLAYER_NAME)

    -- check that the area is not filled with pattern
    assert(res_cmd == false)
    check.not_filled(vec(0, 0, 0), vec(56, 56, 56), pattern)
    check.filled(vec(0, 0, 0), vec(56, 56, 56), "air")

end)
