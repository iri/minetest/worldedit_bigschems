local vec = vector.new


worldedit_bigschems.register_test("Test undo Utils")

worldedit_bigschems.register_test("worldedit_bigschems.undo.transaction rollback", function()

    local trans = worldedit_bigschems.undo.getTransaction("/CMD")

    assert(not worldedit_bigschems.exists(trans:getPath()))

    trans:checkpoint(vec(0,0,0), vec(1,1,1))

    assert(worldedit_bigschems.exists(trans:getPath()))
    assert(worldedit_bigschems.exists(trans:getPath().."/1.mts"))

    trans:rollback()

    assert(not worldedit_bigschems.exists(trans:getPath()))
    assert(not worldedit_bigschems.exists(worldedit_bigschems.transactions_path .. "/" .. trans.folder_name))
    assert(not worldedit_bigschems.exists(worldedit_bigschems.transactions_path .. "/" .. trans.folder_name.."/1.mts"))
    assert(not worldedit_bigschems.exists(worldedit_bigschems.history_path .. "/" .. trans.folder_name))
    assert(not worldedit_bigschems.exists(worldedit_bigschems.history_path .. "/" .. trans.folder_name.."/1.mts"))

end)

worldedit_bigschems.register_test("worldedit_bigschems.undo.transaction commit", function()

    local trans = worldedit_bigschems.undo.getTransaction("/CMD")

    assert(not worldedit_bigschems.exists(trans:getPath()))

    trans:checkpoint(vec(0,0,0), vec(1,1,1))

    assert(worldedit_bigschems.exists(trans:getPath()))
    assert(worldedit_bigschems.exists(trans:getPath().."/1.mts"))

    trans:commit()

    assert(not worldedit_bigschems.exists(trans:getPath()))
    assert(not worldedit_bigschems.exists(worldedit_bigschems.transactions_path .. "/" .. trans.folder_name))
    assert(not worldedit_bigschems.exists(worldedit_bigschems.transactions_path .. "/" .. trans.folder_name.."/1.mts"))

    assert(worldedit_bigschems.exists(worldedit_bigschems.history_path .. "/" .. trans.folder_name))
    assert(worldedit_bigschems.exists(worldedit_bigschems.history_path .. "/" .. trans.folder_name.."/1.mts"))

end)


worldedit_bigschems.register_test("worldedit_bigschems.undo.transaction rollback real", function()


    local pos1 = vec(0,0,0)
    local pos2 = vec(35,35,35)

    local pattern = {testnode1, testnode3}

    local trans = worldedit_bigschems.undo.getTransaction("//TEST")

    for i=0,8,1 do
        for j=0,8,1 do
            for k=0,8,1 do
                local posi_1 = vec(i*4,j*4,k*4)
                local posi_2 = vec(i*4+3,j*4+3,k*4+3)
                trans:checkpoint(posi_1, posi_2)
                place_pattern(posi_1, posi_2, pattern)
            end
        end

    end
    check.filled(pos1, pos2, pattern)

    trans:rollback()

    check.filled(pos1, pos2, { "air" })

    place_pattern(pos1, pos2, { "air" })

end)


worldedit_bigschems.register_test("worldedit_bigschems.undo.transaction commit real", function()


    local pos1 = vec(0,0,0)
    local pos2 = vec(35,35,35)

    local pattern = {testnode1, testnode3}

    local trans = worldedit_bigschems.undo.getTransaction("//TEST")

    for i=0,8,1 do
        for j=0,8,1 do
            for k=0,8,1 do
                local posi_1 = vec(i*4,j*4,k*4)
                local posi_2 = vec(i*4+3,j*4+3,k*4+3)
                trans:checkpoint(posi_1, posi_2)
                place_pattern(posi_1, posi_2, pattern)
            end
        end

    end
    check.filled(pos1, pos2, pattern)

    trans:commit()
   
    check.filled(pos1, pos2, pattern)

    place_pattern(pos1, pos2, { "air" })

end)


