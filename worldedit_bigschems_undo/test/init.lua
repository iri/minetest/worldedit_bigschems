worldedit_bigschems.register_test("worldedit_bigschems_undo tests")
worldedit_bigschems.register_test("is undo dependencies loaded", function()
	assert(worldedit  ~= nil)
	assert(worldedit.pos1  ~= nil)
	assert(worldedit.pos2  ~= nil)
	assert(worldedit.registered_commands  ~= nil)
end)

for _, name in ipairs({
	"undo",
    "commands"
}) do
	dofile(minetest.get_modpath("worldedit_bigschems_undo") .. "/test/" .. name .. ".lua")
end
