local vec = vector.new
local PLAYER_NAME = "Player"

worldedit_bigschems.register_test("Test Big Schems Commands")


worldedit_bigschems.register_test("test worldedit_bigschems.bigmtschemcreate", function()
    local pos1 = vec(0, 0, 0)
    local pos2 = vec(56, 56, 56)

    local export_name = "EXPORT_" .. test_harness.randomString(5)

    worldedit.pos1[PLAYER_NAME] = pos1
    worldedit.pos2[PLAYER_NAME] = pos2

    local pattern = { testnode1, testnode3 }
    place_pattern(pos1, pos2, pattern)

    worldedit_bigschems.bigmtschemcreate(PLAYER_NAME, export_name, vec(10, 10, 10))

    local path = minetest.get_worldpath() .. "/schems/" .. export_name

    local data = worldedit_bigschems.open_file(path .. '/data')

    assert(data ~= false)
    assert(#data == 217)
    for i, mts_item in ipairs(data) do
        if i ~= 1 then
            assert(io.open(path .. '/' .. mts_item.name, "r") ~= nil)
        end
    end
end)

worldedit_bigschems.register_test("test worldedit_bigschems.bigmtschemcreate default val", function()
    local pos1 = vec(0, 0, 0)
    local pos2 = vec(56, 56, 56)

    local export_name = "EXPORT_" .. test_harness.randomString(5)

    worldedit.pos1[PLAYER_NAME] = pos1
    worldedit.pos2[PLAYER_NAME] = pos2

    local pattern = { testnode1, testnode3 }
    place_pattern(pos1, pos2, pattern)


    worldedit_bigschems.bigmtschemcreate(PLAYER_NAME, export_name)

    local path = minetest.get_worldpath() .. "/schems/" .. export_name

    local data = worldedit_bigschems.open_file(path .. '/data')

    assert(data ~= false)
    assert(#data == 2)
    for i, mts_item in ipairs(data) do
        if i ~= 1 then
            assert(io.open(path .. '/' .. mts_item.name, "r") ~= nil)
        end
    end
end)
