-- TODO: don't shit individual variables into the globals
worldedit_bigschems.register_test("worldedit_bigschems_commands tests")
worldedit_bigschems.register_test("is dependencies loaded", function()
	assert(worldedit ~= nil)
	assert(worldedit.pos1 ~= nil)
	assert(worldedit.pos2 ~= nil)
	assert(worldedit.registered_commands ~= nil)
	assert(worldedit_bigschems.undo.state.history ~= nil)
end)

for _, name in ipairs({
	"commands"
}) do
	dofile(minetest.get_modpath("worldedit_bigschems_commands") .. "/test/" .. name .. ".lua")
end
