local S = minetest.get_translator("worldedit_bigschems_commands")

worldedit_bigschems.modpath.worldedit_bigschems_commands = minetest.get_modpath("worldedit_bigschems_commands")

local ml = minetest.settings:get("worldedit_bigschems_max_lengths")
worldedit_bigschems.max_lengths = ml and minetest.parse_json(ml) or { x = 256, y = 256, z = 256 }

dofile(worldedit_bigschems.modpath.worldedit_bigschems_commands .. "/commands.lua")

worldedit.register_command("bigmtschemcreate", {
	params = "<folder_name>",
	description = S("Save the current WorldEdit region using the Minetest Schematic format to \"(world folder)/schems/<folder_name>/*_*_*.mts\""),
	privs = { worldedit = true },
	require_pos = 2,
	parse = function(param)
		if param == "" then
			return false
		end
		--if not check_filename(param) then
		--	return false, "Disallowed file name: " .. param
		--end
		return true, param
	end,
	nodes_needed = worldedit_bigschems.check_region,
	func = worldedit_bigschems.bigmtschemcreate,
})

worldedit.register_command("bigmtschemplace", {
	params = "<folder>",
	description =
	S("Load nodes from \"(world folder)/schems/<folder>.mts\" with position 1 of the current WorldEdit region as the origin"),
	privs = { worldedit = true },
	require_pos = 1,
	parse = function(param)
		if param == "" then
			return false
		end
		-- if not check_filename(param) then
		--	return false, "Disallowed file name: " .. param
		--end
		return true, param
	end,
	func = worldedit_bigschems.bigmtschemplace,
})


if minetest.get_modpath("test_harness") and minetest.settings:get_bool("test_harness_run_tests", false) then
	dofile(worldedit_bigschems.modpath.worldedit_bigschems_commands .. "/test/init.lua")
end
