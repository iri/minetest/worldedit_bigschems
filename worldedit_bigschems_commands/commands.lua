local S = minetest.get_translator("worldedit_bigschems_commands")

function worldedit_bigschems.bigmtschemcreate(name, param, max_lengths)
	max_lengths = max_lengths or worldedit_bigschems.max_lengths

	local path = minetest.get_worldpath() .. "/schems/" .. param
	-- Create directory if it does not already exist
	minetest.mkdir(path)

	local data = {}
	local pos1 = {}
	local pos2 = {}
	local pos_min = {
		x = math.min(worldedit.pos1[name].x, worldedit.pos2[name].x),
		y = math.min(worldedit.pos1[name].y,
			worldedit.pos2[name].y),
		z = math.min(worldedit.pos1[name].z, worldedit.pos2[name].z)
	}
	local pos_max = {
		x = math.max(worldedit.pos1[name].x, worldedit.pos2[name].x),
		y = math.max(worldedit.pos1[name].y,
			worldedit.pos2[name].y),
		z = math.max(worldedit.pos1[name].z, worldedit.pos2[name].z)
	}

	local length_region = { x = pos_max.x - pos_min.x, y = pos_max.y - pos_min.y, z = pos_max.z - pos_min.z, is_bigmtschm = true }
	table.insert(data, length_region)

	local get_len = function(name, i)
		if (length_region[name] - (i * max_lengths[name])) > 0 then
			return max_lengths[name] - 1
		else
			return length_region[name] - ((i - 1) * max_lengths[name])
		end
	end


	for i = 1, math.ceil(length_region.x / max_lengths.x) do
		local len_x = get_len("x", i)
		pos1.x = pos_min.x + (i - 1) * max_lengths.x
		pos2.x = pos1.x + len_x
		for j = 1, math.ceil(length_region.y / max_lengths.y) do
			local len_y = get_len("y", j)
			pos1.y = pos_min.y + (j - 1) * max_lengths.y
			pos2.y = pos1.y + len_y
			for k = 1, math.ceil(length_region.z / max_lengths.z) do
				local len_z = get_len("z", k)
				pos1.z = pos_min.z + (k - 1) * max_lengths.z
				pos2.z = pos1.z + len_z
				local fname = i .. "_" .. j .. "_" .. k .. ".mts"
				local filename = path .. "/" .. fname
				local schm = minetest.create_schematic(pos1, pos2, nil, filename)
				if schm == nil then
					worldedit.player_notify(name, S("Failed to create Minetest schematic"), "error")
					return
				end
				local pos1_save = { x = pos1.x - pos_min.x, y = pos1.y - pos_min.y, z = pos1.z - pos_min.z }
				local pos2_save = { x = pos2.x - pos_min.x, y = pos2.y - pos_min.y, z = pos2.z - pos_min.z }
				table.insert(data, { pos_min = pos1_save, pos_max = pos2_save, name = fname })
			end
		end
	end

	if not worldedit_bigschems.write_file(path .. "/" .. "data", data) then
		worldedit.player_notify(name, S("The function occured an error, the data file is wrong"), "error")
		minetest.chat_send_player(name, worldedit_bigschems.table_unpack(data))
	else
		worldedit.player_notify(name, S("Saved Minetest schematic to @1", param), "info")
	end
	worldedit.prob_list[name] = {}
end

function worldedit_bigschems.bigmtschemplace(name, param)
	local pos_i = worldedit.pos1[name]
	local path = minetest.get_worldpath() .. "/schems/" .. param .. "/"
	local data = worldedit_bigschems.open_file(path .. "data")
	if not data then
		worldedit.player_notify(name,
			S("Error loading @1 data\nIf the error persists, check the file.", path), "error")
		minetest.log("error", "worldedit_bigschems mod: Error when loading " .. path .. "data.")
		return false
	end
	if not data[2].pos_max then
		minetest.chat_send_player(name,S("The folder has been created by a previous version of this mod and can not be used."))
		return false
	end
	if not data[1].is_bigmtschm then
		worldedit.player_notify(name, S("Error, the folder is not a //bigmtschemcreate export."), "error")
		return false
	end
	local cmd = "//bigmtschemplace " .. param .. worldedit_bigschems.table_unpack(pos_i)
	local trans = worldedit_bigschems.undo.getTransaction(cmd)

	for i, v in ipairs(data) do
		if i ~= 1 then
			local pos = { x = v.pos_min.x + pos_i.x, y = v.pos_min.y + pos_i.y, z = v.pos_min.z + pos_i.z }
			local pos2 = { x = v.pos_max.x + pos_i.x, y = v.pos_max.y + pos_i.y, z = v.pos_max.z + pos_i.z }
			if minetest.place_schematic(pos, path .. v.name) == nil then
				worldedit.player_notify(name, S("failed to place Minetest schematic"))
				trans:rollback()
				return
			end
			trans:checkpoint(pos, pos2)
		end
	end

	trans:commit()
	worldedit.player_notify(name, S("Placed Minetest schematic @1",param))
	return true
end
