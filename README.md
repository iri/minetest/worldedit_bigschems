# worldedit_bigschems

WorldEdit-BigSchems is a mod that extends the functionality of the WorldEdit mod by adding four new commands. These commands allow users to create and place large schematics, as well as maintain a command journal for undoing past actions.

They were used as part of the [UNEJ project](https://minetest.net/education/), which uses Minetest with students to rework on real areas in France. This was made possible thanks to the [IGN (French  National Institute of Geographic and Forest Information) service](https://minecraft.ign.fr/), which allows to create Minetest maps from from geographical data of France or the world (OSM data).

**Requirements**: WorldEdit mod must be installed.

## Commands

### `//bigmtschemcreate \<folder_name\>`

This command creates a folder within the schematics directory of the WorldEdit area (which must already be set up).  
It functions the same as the `//mtschemcreate` command from WorldEdit, but with the ability to create large schematics.

---

### `//bigmtschemplace \<folder_name\>`

This command imports a schematic created by the `//bigmtschemcreate` command.  
The import starts from the `pos1` position in WorldEdit, which will serve as the bottom-left corner of the schematic.  
It is similar to the `//mtschemplace` command, but allows for placing large schematics.

---

### `//worldjournal`

Displays a list of logged actions that can be undone.

---

### `//worldundo [ID]`

Undoes the actions from `//bigmtschemplace` command.  
The ID corresponds to the log ID shown by the `//worldjournal` command. ID `1` represents the most recent action.

---

## License

Licensed under the AGPL (Affero General Public License).

## Funding

This mod is published with [funding from the NLNet Foundation](https://nlnet.nl/project/MinetestEdu/).
