worldedit_bigschems.open_file = function(path, warn)
	warn = (warn==nil) or warn
	local file = io.open(path,"r")
	if not file then
		if warn then
			minetest.log("warning","UNEJ mod -!- failed opening " .. path .. ".")
		end
		return false
	end
	local content = file:read("*all")
	file:close()
	local data = minetest.parse_json(content)
	if not data then
		minetest.log("error","UNEJ mod -!- Failed to convert json to table " .. path .. "\nCheck that this file is a json file.")
		return false
	end
	return data
end

worldedit_bigschems.write_file = function(path, data)
	local file = io.open(path,"w+")
	if not file then
		return false
	end
	file:write(minetest.write_json(data,true))
	file:close()
	return true
end

function worldedit_bigschems.check_region(name)
	return worldedit.volume(worldedit.pos1[name], worldedit.pos2[name])
end

local sets = {{97, 122}, {65, 90}, {48, 57}} -- a-z, A-Z, 0-9

function worldedit_bigschems.random_string(length)
	local str = ""
	for i = 1, length do
		math.randomseed(os.clock() ^ 5)
		local charset = sets[ math.random(1, #sets) ]
		str = str .. string.char(math.random(charset[1], charset[2]))
	end
	return str
end

function worldedit_bigschems.dump(o)
	if type(o) == 'table' then
	   local s = '{ '
	   for k,v in pairs(o) do
		  if type(k) ~= 'number' then k = '"'..k..'"' end
		  s = s .. '['..k..'] = ' .. worldedit_bigschems.dump(v) .. ','
	   end
	   return s .. '} '
	else
	   return tostring(o)
	end
 end
 

function worldedit_bigschems.exists(path)
    local file = io.open(path,"r")
    if (file ~= nil) then
        io.close(file)
        return true
    else
        return false
    end
end

-- from https://devforum.roblox.com/t/functional-shenanigans-map-filter-partition-reduce-two-ways/199027
function worldedit_bigschems.reduce(sequence, operator, sink)
    if #sequence == 0 then
        return nil
    end
    local out = sink
    for i=1,#sequence do
        out = operator(out, sequence[i])
    end
    return out
end

function worldedit_bigschems.translateForUser(text, name)
	local info = minetest.get_player_information(name)
	local language = info and info.language or "en"
	return minetest.get_translated_string(language, text)
end

function worldedit_bigschems.table_unpack(data)
	if type(data) ~= "table" then
		return tostring(data)
	end
	local str = "{"
	local i = true
	for k,v in pairs(data) do
		if i then
			str = str .. "\""..k.."\":"..worldedit_bigschems.table_unpack(v)
			i=false
		else
			str = str .. ", \""..k.."\":"..worldedit_bigschems.table_unpack(v)
		end
	end
	str = str .. "}"
	return str
end
