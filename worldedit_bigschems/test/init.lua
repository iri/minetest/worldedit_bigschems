---------------------
-- The actual tests
---------------------
worldedit_bigschems.register_test = test_harness.get_test_registrator("worldedit_bigschems", worldedit_bigschems.version_string)

worldedit_bigschems.register_test("is area loaded?", function()
	local pos1, _ = area.get(1)
	assert(minetest.get_node(pos1).name == "air")
end)

dofile(worldedit_bigschems.modpath.worldedit_bigschems.."/test/utils.lua")
