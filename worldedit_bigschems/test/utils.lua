worldedit_bigschems.register_test("Test utils")

worldedit_bigschems.register_test("test exists", function()
    local path = worldedit_bigschems.modpath.worldedit_bigschems
    assert(worldedit_bigschems.exists(path))
    assert(worldedit_bigschems.exists(path.."/init.lua"))
    local wrong_path = worldedit_bigschems.modpath.worldedit_bigschems .. worldedit_bigschems.random_string(10)
    assert(not worldedit_bigschems.exists(wrong_path))
end)
