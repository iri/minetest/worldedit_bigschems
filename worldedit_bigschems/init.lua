worldedit_bigschems = {}

worldedit_bigschems.modpath = {}

worldedit_bigschems.modpath.worldedit_bigschems = minetest.get_modpath('worldedit_bigschems')

local ver = { major = 0, minor = 1 }
worldedit_bigschems.version = ver
worldedit_bigschems.version_string = string.format("%d.%d", ver.major, ver.minor)


dofile(worldedit_bigschems.modpath.worldedit_bigschems .. "/utils.lua")

if minetest.get_modpath("test_harness") and minetest.settings:get_bool("test_harness_run_tests", false) then
    dofile(worldedit_bigschems.modpath.worldedit_bigschems .. "/test/init.lua")
end
